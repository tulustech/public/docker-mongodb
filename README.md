## Docker Compose File for MongoDB

### Configure
```
$ cp sample.env .env
$ vim .env
```


### To run
```
$ docker-compose up -d
```


### To shutdown
```
$ docker-compose down
```
